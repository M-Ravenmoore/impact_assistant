# impact_assistant

## Version

### Development branch version

0.1.1
[gitpages dev Branch](https://gitlab.com/M-Ravenmoore/impact_assistant/-/tree/dev?ref_type=heads)

### Production Branch Version

0.1.0
[gitpages Main Branch](https://gitlab.com/M-Ravenmoore/impact_assistant/-/tree/main?ref_type=heads)

## Description

A Cross platform assistant for the IATSE local 15 members use with our online dispatch service.

It is a combination scrape and autimation system for a basic daily task of looking for work.

## Installation

Soon to come

### Programers

 (See contributing to code)

## Visuals

soon to come

## Support

email the developer at <m.ravenmoore@gmail.com>
or submit a issue on [gitlab](https://gitlab.com/M-Ravenmoore/auto-ui/-/issues)

## Roadmap

This is a testing prototype as of 6/15/2024 and will nolonger be added to substantialy but will be maintained for the next 5 to 10 weeks while data is collected.

## Contributing to Code

I am open to outside contributions please look at the open issues on the [issues board](https://gitlab.com/M-Ravenmoore/impact_assistant/-/issues)
Then follow the instructions below

### instructions to add to code

- fork and clone the code from the repo
- set up your env for development detail can be found on Flutters page see [referances](./readme_docs/referances.md)
- create branch for your issue (branch names are in issue details)
- make your code changes documenting your work
- push code to and submit an request for merge.

## Authors and acknowledgment

Matthew Ravenmoore (Developer)
Chris Hudson (Mentor helping with code and packageing)

Further acknowledgments can be found [here](docs/referances.md)

## License

MIT Licence

## Project status

In active open development
