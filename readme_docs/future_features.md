# Future Features

The plans for the next version of this program are currently

## sort feature

### sort_settings functions

the goals of sort functions are the following use cases

- as a user i want to see only what matches my open schedule and i can bid on nothing else
- as a user i want to see every position that meets my schedule
- as a user i want to see every position that matches a list of titles
- as a user i want to see only positions that matches a list of titles and i can bid on

## Error Handeling

the software needs error handeling for:

- login (if no credents or wrong credents)

## settings

### test mode

as a dev i want a setting to turn on to restrict data pulls durring testing to speed up testing results.

### Theme

as a user i would like to be able to change the color theme

## Timer

i would like to add a timer that times the return speed from click of "get work" or "bid for work" to results.

## Reminder feature

- users will then be reminded that priority is not set you will have to arange them on your list but they will be in date order for the more part
- provide users with link and save options as well as exit the program

## save log feature

- as a user i would like to see and a beable to save a log of what has been bid on for record keeping.
